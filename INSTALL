$Id: INSTALL,v 1.13 2003/12/05 09:30:44 amitshah Exp $

AudioLink

Read this file before you use AudioLink. It documents the way(s) you
can setup AudioLink and get it working and run the scripts that come
with AudioLink. Since AudioLink uses a MySQL database to store its
data, you need to setup MySQL too. All the necessary instructions,
with external references if needed, are given in this document.

Prerequisites:
~~~~~~~~~~~~~
	Perl
	   to run the scripts

	MySQL-client
	   to connect to the database

	MySQL-server
	   if you want to maintain songs database on your machine

	DBD::mysql, DBI Perl modules
	   (libdbd-mysql-perl, libdbi-perl on Debian)

	MP3::Info Perl module
	   (libmp3-info-perl on Debian)

	Ogg::Vorbis::Header Perl Module
	   (libogg-vorbis-header-perl)

Perl and MySQL shouldn't be a problem to get and install. Getting
mysql-perl also shouldn't be a hassle. On RedHat-based systems, it is
called perl-dbd-mysql-xxx.rpm (where xxx is the version number of the
package). For the libmp3-info-perl, on rpm-based distributions, you
could use the rpms from

	http://inconnu.isu.edu/~ink/new/projects/xchat-xmms/

If your distribution doesn't have one.

On Debian-based systems, it's very easy:

	apt-get install libmp3-info-perl


If you don't get the packages, you can always get the sources from 

	http://www.cpan.org/modules/by-module/MP3/


Unofficial Debian packages for Debian sid (i386) systems for
Ogg::Vorbis::Header can be obtained by putting this line in your
/etc/apt/sources.list:

	deb http://mentors.debian.net/debian unstable main

and install it by running
	apt-get install libogg-vorbis-header-perl

If you run a non-i386 machine, you can get the sources by putting this
line:

	deb-src http://mentors.debian.net/debian unstable main

and compiling the module from sources.

If you run the Debian stable/testing distros, some package
dependencies will need to be revised. Send me links if you package it
for stable/testing; I can't because I don't have access to
stable/testing machines.

Note: all the perl modules mentioned above are available from CPAN
(www.cpan.org).


Installing and running:
~~~~~~~~~~~~~~~~~~~~~~
You can directly run the scripts from the code/ directory. You can
also run 'make install' to install these scripts system-wide, so that
all the users on the local machine have access to them. You will need
to run 'make install' so that the audiolink script works properly and
the databases and tables are set up properly the first time you use
AudioLink.


Setting up the database for first use:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
You'll have to create a database in MySQL before you use
AudioLink. The audiolink script assists you in creating the database,
set up tables and also enter frequently used values in a configuration
file.

Run the 'audiolink' script once you've installed AudioLink. This
script will ask you certain questions related to logging into the
database. After that, it proceeds to create the database and setup the
tables.

If you want to create the database manually, see the following section.

Now populate the database with the code/alfilldb script.

More documentation on the usage, etc. can be found in the manpages and
the Documentation/ directory.

Send feedback to audiolink-users@lists.sourceforge.net

Have fun :-)


Creating the MySQL database and tables manually:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
If you want to create the AudioLink database manually, run the
following from the shell:

(Assuming the user for the database is root. Change it to suit your
environment).

mysqladmin -u root -p create aldb

This command will prompt you for the root password (for MySQL, not the
system-wide root password). If you have not assigned a password, omit
the -p option in the command line.

After creating the database, create a table. You can use the sample
schema given in db/mysql.schema directory. Edit it if needed, and
create the table by:

mysql -u root -p aldb < mysql.schema

This will create the table for you.
